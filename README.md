# daverona/python

This is a Python package template.

## Quick Start

For testing, make a directory and go into it to run:

```bash
pip3 install git+ssh://git@gitlab.com/daverona/templates/python.git
aeon-says
```

If "Hello, World!" message shows up, it's working.

Make `index.py` with:

```python
from daverona.aeon import hello

hello.say()
```

And run:

```bash
python3 index.py
```

The "Hello, World!" message should show up again.

Since both of the executable script (`aeon-says`) and the package module (`daverona.aeon.hello`) are working, let's uninstall:

```bash
pip3 uninstall --yes aeon
```

## Development

It's time to make this template yours. Make your own repository by *importing* not forking this template.
If you don't know what importing means, copy this template to your own repository.
We assume that you have a repository exactly same as this template.

Clone the newly created repository to your local machine if you haven't done so already.
Change directory into the cloned directory and install the package in *edit* mode:

```bash
pip3 install --editable .
```

To verify if it's installed in edit mode:

```
$ pip3 list
Package                     Version    Location           
--------------------------- ---------- -------------------
...
aeon                        0.0.1      /path/
...
```

Once you are comfortable with your own package, review `setup.py` in the package for customization.
Before customize `setup.py`, remove the package from :

```bash
pip3 uninstall --yes aeon
```

## Production

Please read [https://gitlab.com/daverona/docs/python/-/blob/master/repo.md](https://gitlab.com/daverona/docs/python/-/blob/master/repo.md).

## References

* Native namespace package: [https://packaging.python.org/guides/packaging-namespace-packages/#native-namespace-packages](https://packaging.python.org/guides/packaging-namespace-packages/#native-namespace-packages)
* Building and Distributing Packages with Setuptools: [https://setuptools.readthedocs.io/en/latest/setuptools.html](https://setuptools.readthedocs.io/en/latest/setuptools.html)
